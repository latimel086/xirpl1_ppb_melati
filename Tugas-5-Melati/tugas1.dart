import 'dart:io';

void main() {
  // Input data siswa
  stdout.write("Nama siswa\t: ");
  String namaSiswaMelati = stdin.readLineSync()!;
  stdout.write("Kelas Siswa\t: ");
  String kelasSiswaMelati = stdin.readLineSync()!;
  stdout.write("Nilai Harian\t: ");
  double nilaiHarianMelati = double.parse(stdin.readLineSync()!);
  stdout.write("Nilai PTS\t: ");
  double nilaiPTSMelati = double.parse(stdin.readLineSync()!);
  stdout.write("Nilai Praktek\t: ");
  double nilaiPraktekMelati = double.parse(stdin.readLineSync()!);
  stdout.write("Nilai PAS\t: ");
  double nilaiPASMelati = double.parse(stdin.readLineSync()!);

  // Menghitung nilai akhir
  double nilaiAkhirMelati = (nilaiHarianMelati + nilaiPTSMelati + nilaiPraktekMelati + nilaiPASMelati) / 4;

  // Menentukan status siswa
  String statusSiswaMelati = (nilaiAkhirMelati >= 78)
      ? "Lulus, naik fase berikutnya"
      : "Remedial, bisa naik fase berikutnya setelah menyelesaikan fase sebelumnya";

  // Menampilkan hasil
  print("\nNilai Akhir\t: ${nilaiAkhirMelati.toStringAsFixed(2)}");
  print("Status Siswa\t: $statusSiswaMelati");
}