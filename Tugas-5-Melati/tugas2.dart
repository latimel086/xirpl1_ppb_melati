import 'dart:io';

void main() {
  // Input data belanja
  stdout.write("ID Beli\t: ");
  String idBeliMelati = stdin.readLineSync()!;
  stdout.write("Kode Barang\t: ");
  String kodeBarangMelati = stdin.readLineSync()!;
  stdout.write("Nama Barang\t: ");
  String namaBarangMelati = stdin.readLineSync()!;
  stdout.write("Harga Barang\t: ");
  double hargaBarangMelati = double.parse(stdin.readLineSync()!);
  stdout.write("Jumlah Beli\t: ");
  int jumlahBeliMelati = int.parse(stdin.readLineSync()!);

  // Menghitung jumlah bayar
  double jumlahBayarMelati = hargaBarangMelati * jumlahBeliMelati;

  // Menghitung diskon
  double diskon = 0;
  if (jumlahBayarMelati > 5000000) {
    diskon = jumlahBayarMelati* 0.10;
  } else if (jumlahBayarMelati > 1000000) {
    diskon = jumlahBayarMelati * 0.07;
  } else if (jumlahBayarMelati > 400000) {
    diskon = jumlahBayarMelati * 0.05;
  } else if (jumlahBayarMelati > 200000) {
    diskon = jumlahBayarMelati* 0.03;
  }

  // Menghitung total bayar
  double totalBayar = jumlahBayarMelati - diskon;

  // Menampilkan hasil
  print("\nJumlah Bayar\t: \$${jumlahBayarMelati.toStringAsFixed(2)}");
  print("Diskon\t: \$${diskon.toStringAsFixed(2)}");
  print("Total Bayar\t: \$${totalBayar.toStringAsFixed(2)}");
}