import 'dart:io';

void main() {
  stdout.write("Mau menginstall aplikasi? (Y/T): ");
  var input = stdin.readLineSync();

  var message = (input != null && input.toLowerCase() == 'y') ? "Anda akan menginstall aplikasi Dart" : "Aborted";
  
  print(message);
}