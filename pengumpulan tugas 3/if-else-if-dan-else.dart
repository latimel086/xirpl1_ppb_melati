import 'dart:io';

void main() {
  stdout.write("Masukkan nama: ");
  String namaMelati = stdin.readLineSync()!;
  
  stdout.write("Masukkan peran (Penyihir, Guard, Werewolf): ");   
  String peranMelati = stdin.readLineSync()!;

  if (namaMelati.isEmpty && peranMelati.isEmpty) {
    print("Nama harus diisi!");
  } else if (namaMelati.isNotEmpty && peranMelati.isEmpty) {
    print("Halo $namaMelati, Pilih peranmu untuk memulai game!");
  } else {
    print("Selamat datang di Dunia Werewolf, $namaMelati");
    if (peranMelati == "Penyihir") {
      print("Halo Penyihir $namaMelati, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peranMelati == "Guard") {
      print("Halo Guard $namaMelati, kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else if (peranMelati == "Werewolf") {
      print("Halo Werewolf $namaMelati, Kamu akan memakan mangsa setiap malam!");
    } else {
      print("Peran yang dimasukkan tidak dikenali.");
    }
  }
}