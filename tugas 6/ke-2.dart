void main(melati){
// Definisikan function kalikan dengan dua parameter num1 dan num2
int kalikan(int num1, int num2) {
  // Kembalikan hasil perkalian dari num1 dan num2
  return num1 * num2;
}
  // Tentukan dua bilangan untuk dikalikan
  var num1 = 12;
  var num2 = 4;

  // Panggil function kalikan dan simpan hasilnya di variabel hasilKali
  var hasilKali = kalikan(num1, num2);

  // Cetak hasil perkalian
  print(hasilKali); // Output: 48
}