import 'dart:io';
void main(Melati) {
  stdout.write('Masukkan angka untuk dihitung faktorial: ');
  int number = int.parse(stdin.readLineSync()!);

  int result = factorial(number);
  print('$number! = $result');
}

int factorial(int n) {
  if (n <= 0) {
    return 1;
  } else {
    int result = 1;
    for (int i = 1; i <= n; i++) {
      result *= i;
    }
    return result;
  }
}
