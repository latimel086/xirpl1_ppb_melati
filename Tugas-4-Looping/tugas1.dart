void main() {
  print("LOOPING PERTAMA");

  var aMelati = 2;
  while (aMelati <= 20) {
    print("$aMelati I love coding");
    aMelati += 2;
  }

  print("LOOPING KEDUA");

  var bMelati = 20;
  while (bMelati >= 2) {
    print("$bMelati I will become a mobile developer");
    bMelati -= 2;
  }
}